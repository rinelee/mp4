package mp4;

import java.util.HashMap;

public class Movie {
	private final int id;
	private final String name;
	private final int releaseYear;
	private final String imdbUrl;

	private static HashMap<Integer, Movie> moviesByID= new HashMap<Integer, Movie>();
	private static HashMap<String, Integer> moviesByName = new HashMap<String, Integer>();
	private HashMap<Integer,Integer> reviews;
	
	/**
	 * Create a new Movie object with the given information.
	 * 
	 * @param id
	 *            the movie id. Requires that this value is unique to each movie.
	 * @param name
	 *            the name of the movie. Requires this to be unique to each movie.
	 * @param releaseYear
	 *            the year of the movie's release
	 * @param imdbUrl
	 *            the movie's IMDb URL
	 */
	public Movie(int id, String name, int releaseYear, String imdbUrl) {
		this.id = id;
		this.name = name;
		this.releaseYear = releaseYear;
		this.imdbUrl = imdbUrl;
		this.reviews = new HashMap<Integer, Integer>();
		
		moviesByID.put(id, this);	
		moviesByName.put(name, id);
	}

	/**
	 * Obtains a movie object using its ID.
	 * 
	 * @param id
	 * 			The unique ID of the movie.
	 * @return
	 * 			The movie the ID corresponds to, provided that there is an exact match.
	 * @throws NoSuchMovieException
	 * 			If the no movie of this exact ID exists.
	 */
	public static Movie retrieveMovieByID(int id) throws NoSuchMovieException{
		if(!moviesByID.containsKey(id)){
			throw new NoSuchMovieException();
		}
		return moviesByID.get(id);
	}
	/**
	 * Return the name of the movie
	 * 
	 * @return movie name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Returns the movie id.
	 * 
	 * @return movie id.
	 */
	public int getID(){
		return id;
	}
	
	/**
	 * hashCode for equality testing
	 */
	@Override
	public int hashCode() {
		return id;
	}
	
	/**
	 * Method to check if two Movie objects are equal. Movies are equal if they have the same ID.
	 * 
	 * @return	
	 * 		True if the objects are equal; false otherwise.
	 * 
	 */
	@Override
	public boolean equals(Object other) {
		
		if(other == null){
			return false;
		}
		
		if(other.getClass() != Movie.class){
			return false;
		}
		
		return equals((Movie) other); 
	}
	
	public boolean equals (Movie other){
		
		if(other == this){ //Reference equality
			return true;
		}
		
		if(this.hashCode() != other.hashCode()){
			return false;
		}
		
		return true;
	}
	
	/**
	 * Returns a map of user id's and their reviews of the movie.
	 * 
	 * @return
	 * 		a map whose keys are user id's and values are the user reviews.
	 */
	public HashMap<Integer, Integer> getReviews(){
		
		return this.reviews;
	}
	
	
	
	/**
	 * Adds a review to the movie.
	 * 
	 * @requires 
	 * 			ratingToAdd is a rating of the movie.
	 * @param ratingToAdd
	 * 			the rating to include for this move.
	 * @returns 
	 * 			true if the rating was successfully added, false otherwise.
	 */
	public boolean addReview(Rating ratingToAdd){
				int userID = ratingToAdd.getUserId();
				int ratingVal = ratingToAdd.getRating();
				
	
				if(reviews.containsKey(userID)){
					return false;
				}
				
				reviews.put(userID, ratingVal);
				return true;
		
	}

	
}
