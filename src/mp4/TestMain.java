package mp4;

import java.io.IOException;
import java.util.List;
// This class creates and illustrates the use of similarity graphs.

public class TestMain {
	
	//Test Shortened Data Set
	/**
	 * @param args no arguments needed.
	 * @throws NoSuchMovieException 
	 * 
	 */
	public static void main(String[] args) throws IOException, NoSuchMovieException {
		String movieFilea = "data/u.itemshort.txt";
		String imbdFilea = "data/u.infoshort.txt";
		SimilarityGraph similarityGrapha = new SimilarityGraph(imbdFilea, movieFilea);
		
		int valuea = 0;
		
		try {
			valuea = similarityGrapha.getShortestPathLength(similarityGrapha.getMovieId("Unlimited Blade Works"), similarityGrapha.getMovieId("Heavens Feel"));
		} catch (NoSuchMovieException e) {
			System.out.println("Fail");
		} catch (NoPathException e) {
			System.out.println("Fail");
		}
		
		System.out.println("Shortest Path Length: " + valuea);
		System.out.println("Expected Path Length: 2");
		System.out.println();
		

		List<Movie> shortestPatha = null;
		try {
			shortestPatha = similarityGrapha.getShortestPath(similarityGrapha.getMovieId("Unlimited Blade Works"), similarityGrapha.getMovieId("Heavens Feel"));
		} catch (NoPathException e1) {
		}
		
		System.out.print("Shortest Path: ");
		for(Movie currMovie: shortestPatha){
			System.out.print(currMovie.getName() + " ");
		}

		System.out.println();
		System.out.println("Expected Path: Unlimited Blade Works Heavens Feel");
		System.out.println();
		
		
		//Test True Data Set
		
		
		String movieFile = "data/u.item.txt";
		String imbdFile = "data/u.data.txt";
		System.out.println();
		SimilarityGraph similarityGraph = new SimilarityGraph(imbdFile, movieFile);
			
		int value = 0;
		
		try {
			value = similarityGraph.getShortestPathLength(similarityGraph.getMovieId("Jean de Florette"), similarityGraph.getMovieId("Private Benjamin"));
		} catch (NoSuchMovieException e) {
			System.out.println("Fail");
		} catch (NoPathException e) {
			System.out.println("Fail");
		}
		
		System.out.println("Shortest Path Length between Jean de Florette (source) and Private Benjamin (target):" + value);

		try {
			value = similarityGraph.getShortestPathLength(similarityGraph.getMovieId("Private Benjamin"), similarityGraph.getMovieId("Jean de Florette"));
		} catch (NoSuchMovieException e) {
			System.out.println("Fail");
		} catch (NoPathException e) {
			System.out.println("Fail");
		}
		
		System.out.println("Shortest Path Length between  Private Benjamin (source) and Jean de Florette (target):" + value);
		System.out.println("Expected Output: Both path lengths should be equal.");
		
		List<Movie> shortestPath = null;
		try {
			shortestPath = similarityGraph.getShortestPath(similarityGraph.getMovieId("Private Benjamin"), similarityGraph.getMovieId("Jean de Florette"));
		} catch (NoPathException e1) {
		}
		
		System.out.println();
		System.out.print("Shortest Path  between  Private Benjamin (source) and Jean de Florette (target): ");
		for(Movie currMovie: shortestPath){
			System.out.print(currMovie.getName() + " ");
		}
		
		
	}

}
