package mp4;

import java.util.HashMap;

/**
 * A class representing a Vertex object.
 * 
 * @author Catherine Lee
 *
 */
public class Vertex {
	//AF(r) = A vertex of a movie graph.
	
	
	//Rep Invariant: directPath and hasPathTo are never null
	//All movies that the vertex has a path to are listed in hasPathTo.
	//All nodes adjacent to the vertex are mapped to directPath with the other vertex and the edge.
	
	private Movie movie;
	private HashMap<Vertex, Integer> directPath;
	
	private double minDistance;
	private Vertex previousNode;
	
	/**
	 * @requires
	 * 			movie is not null
	 * @param movie
	 * 			the movie represented by this vertex.
	 */
	public Vertex(Movie movie){
		this.movie = movie;
		directPath = new HashMap<Vertex, Integer>();
	}
	
	/**
	 * Returns the movie representation of this vertex.
	 */
	public Movie getMovie(){
		return movie;
	}
	
	/**
	 * Records the previous node visited.
	 * 
	 * @param previous
	 * 			the previous node visited.
	 */
	public void setPreNode(Vertex previous){
		previousNode = previous;
	}
	
	/**
	 * Returns the last visited node in a path.
	 * 
	 * @return
	 * 		the previously stored node value. Null if there is no node stored.
	 */
	public Vertex getPreNode(){
		return previousNode;
	}
	/**
	 * Sets a temporary distance. Used for finding the shortest distance/path.
	 * 
	 * @param newDistance
	 * 			value to set the distance to.
	 */
	public void setDistance(double newDistance){
		minDistance = newDistance;
	}
	
	/**
	 * Returns the current tentative minimum distance of the node.
	 * 
	 * @return
	 * 		the temporary distance saved.
	 */
	public double getDistance(){
		return minDistance;
	}
	
	/**
	 * Adds an edge to this vertex.
	 * 
	 * @param other
	 * 			the other vertex at the end of the edge.
	 * @param edge
	 * 			the edge to add.
	 * @return	
	 * 		true if the add was successful, false if the edge already exists.
	 */
	public boolean addEdge(Vertex other, int weight){
		if(directPath.containsKey(other)){
			return false;
		}
		
		directPath.put(other, weight);
		
		return true;
	}
	
	
	/**
	 * Returns a map of vertices that this vertex is connected to mapped to its weight.
	 * 
	 * @return
	 * 		a map whose keys are vertices adjacent to this vertex, and values are the weight/length of the edge.
	 */
	public HashMap<Vertex, Integer> getDirectPaths(){
		return new HashMap<Vertex, Integer>(directPath);
	}

	/**
	 * Tests for object equality.
	 * 
	 * @param other
	 * 		the object to test for equality.
	 * @return
	 * 		true if the objects are equal, false otherwise.
	 */
	public boolean equals(Object other){
		//Case 1: Not the same class and therefore not equal.
		if(!(other instanceof Vertex)){
			return false;
		}
		
		return equals((Vertex) other);
	}
	
	public boolean equals(Vertex other){
		if(other == null){
			return false;
		}
		
		if(other == this){
			return true;
		}
		
		if(hashCode() != other.hashCode()){
			return false;
		}
		
		return true; //Vertexes are the same if their movie are the same, therefore we only need to check the
					//hashCode in this case.
	}
	
	/**
	 * hashCode for equality testing.
	 */
	public int hashCode(){
		return movie.getID();
	}
	
}
