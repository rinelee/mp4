package mp4;


import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class SimilarityGraph {
	    //An immutable class representing a similarity graph of movies as vertices and ratings as edges. 
		//Every movie is connected to every other movie in the graph.

		//Abstraction Function:
		//AF(r) = a graph, graphValues, such that every vertex on the graph is recorded and mapped to
		//every edge connecting it to another vertex. Two SimilarityGraphs are equal if they have the
		//same values mapped to the same edges.

		//Rep Invariants:
		//similarityGraph != null
		//Every edge in the graph represented by similarityGraph is represented by the dissimilarity between the two vertexes(movies)
	    //Every movie (vertex) is mapped to every other movie in the graph.
	
		//Note: The rep will not be checked after all mutator methods, as this can be expensive with 
		//a thousand movies.
		
	 	
		private	MovieGraph similarityGraph = new MovieGraph();
		
		/**
		 * Generates a similarity graph for movies using the provided ratings and movie files.
		 * 
		 * @requires
		 * 		 	imbdFile and movieFile are not null and are valid ratings and movie files respectively.
		 * @param imbdFile
		 * 			The file containing the ratings of movies in the similarity graph.
		 * @param movieFile
		 * 			The movies that will act as vertices of the graph.
		 * @throws IOException
		 * 			If either the imbdFile or the movieFile cannot be located.
		 * @throws NoSuchMovieException 
		 * 			If a movie in ratings does not exist in movieFile.
		 */
		public SimilarityGraph(String imbdFile, String movieFile) throws IOException, NoSuchMovieException{
			System.out.println("Creating a similarity graph from movie file " + movieFile + " and ratings file " + imbdFile + ".");
			System.out.println();
			
			HashSet<Movie> listOfMovies = new HashSet<Movie>();
		
			System.out.println("Generating Vertices...");
			MovieIterator iter = new MovieIterator(movieFile);
			while ( iter.hasNext() ) {
				Movie movie = iter.getNext();
				listOfMovies.add(movie);
			}
			
			System.out.println("Vertices complete.");
			System.out.println();
			System.out.println("Generating Edges...");
			
			addAllRatings(listOfMovies, imbdFile);
			
			
			for(Movie sourceMovie: listOfMovies){
				for(Movie targetMovie: listOfMovies){
					if(!targetMovie.equals(sourceMovie)){
						addEdge(sourceMovie, targetMovie);
						}
				}
			}
			
			System.out.println("Edges complete.");
			System.out.println();
		}
		
		
		//Adds the ratings from the IMBD file to their respective movies.
		//
		//Requires: listOfMovies and imbdFile are not null, and imbdFile is contains ratings of movies that are elements in listOfMovies.
		//Throws: IOException when imbdFile cannot be read, and NoSuchMovieException if the movie stated does not exist.
		private void addAllRatings(HashSet<Movie> listOfMovies, String imbdFile) throws IOException, NoSuchMovieException{
			RatingIterator iter2 = new RatingIterator(imbdFile);
			
			while (iter2.hasNext()) {
				Rating rating = iter2.getNext();
				Movie currMovie = Movie.retrieveMovieByID(rating.getMovieId());
				currMovie.addReview(rating);
			}
			
		}
		
		
		//Adds an edge to two specified movies.
		//
		//Param movie1: One vertex of the edge.
		//Param movie2: The other vertex of the edge.
		//Requires: movie1 and movie 2 are not null.
		//Returns: true if the edge was successfully added, false otherwise.
		private boolean addEdge(Movie movie1, Movie movie2) {
			
			int edgeWeight = getEdgeWeight(movie1,movie2);
			return similarityGraph.addEdge(movie1, movie2, edgeWeight);
		}
			

		/**
		 * Return the length of the shortest path between two movies in the graph.
		 * Throws an exception if the movie ids do not represent valid vertices in
		 * the graph.
		 * 
		 * @param moviedId1
		 *            the id of the movie at one end of the path.
		 * @param moviedId2
		 *            the id of the movie at the other end of the path.
		 * @throws NoSuchMovieException
		 *             if one or both arguments are not vertices in the graph.
		 * @throws NoPathException
		 *             if there is no path between the two vertices in the graph.
		 * 
		 * @return the length of the shortest path between the two movies
		 *         represented by their movie ids.
		 */
		public int getShortestPathLength(int movieId1, int movieId2)
				throws NoSuchMovieException, NoPathException {
			
			return similarityGraph.getShortestPathLength(movieId1, movieId2);
		
		}

		/**
		 * Return the shortest path between two movies in the graph. Throws an
		 * exception if the movie ids do not represent valid vertices in the graph.
		 * 
		 * @param moviedId1
		 *            the id of the movie at one end of the path.
		 * @param moviedId2
		 *            the id of the movie at the other end of the path.
		 * @throws NoSuchMovieException
		 *             if one or both arguments are not vertices in the graph.
		 * @throws NoPathException
		 *             if there is no path between the two vertices in the graph.
		 * 
		 * @return the shortest path, as a list, between the two movies represented
		 *         by their movie ids. This path begins at the movie represented by
		 *         movieId1 and ends with the movie represented by movieId2.
		 */
		public List<Movie> getShortestPath(int movieId1, int movieId2)
				throws NoSuchMovieException, NoPathException {
			
			return similarityGraph.getShortestPath(movieId1, movieId2);
			
		}

		/**
		 * Returns the movie id given the name of the movie. For movies that are not
		 * in English, the name contains the English transliteration original name
		 * and the English translation. A match is found if any one of the two
		 * variations is provided as input. Typically the name string has <English
		 * Translation> (<English Transliteration>) for movies that are not in
		 * English.
		 * 
		 * @param name
		 *            the movie name for the movie whose id is needed.
		 * @return the id for the movie corresponding to the name. If an exact match
		 *         is not found then return the id for the movie with the best match
		 *         in terms of translation/transliteration of the movie name. Note that an
		 *         exact match of the translation/transliteration  or the start of the English name 
		 *         is required.
		 * @throws NoSuchMovieException
		 *             if the name does not match any movie in the graph or is an empty string.
		 */
		public int getMovieId(String name) throws NoSuchMovieException {
			return similarityGraph.getMovieId(name);
		}
		
		 // Calculates the weight of the edge between two movies based on a dissimilarity score, where the dissimilarity score
		 // corresponds to 1 + total number of reviewers - ((size of the intersection of movie1's likers and movie2's likers) + 
		 // (size of the intersection of movie1's dislikers and movie2's dislikers)).
		 //
		 // Param movie1: the source vertex.
		 // Param movie2: the other vertex.
		 // Requires: movie1 and movie2 are not null and have reviews.
		 // Returns: the weight of the edge/dissimilarity score of the movies.
		private int getEdgeWeight(Movie movie1, Movie movie2) {
			int totalReviewers;
			int likes = 0;
			int dislikes = 0;
			
			Map<Integer,Integer> movie1Reviews = movie1.getReviews();
			Map<Integer,Integer> movie2Reviews = movie2.getReviews();
			
			Set<Integer> movie1Reviewers = movie1Reviews.keySet();
			totalReviewers = movie1Reviewers.size() + movie2Reviews.size(); //Ignore common reviewers for now.
			
			for(int userID: movie1Reviewers){
				
				if(movie2Reviews.containsKey(userID)){
					totalReviewers--; //Remove the duplicated reviewer
					
					if(movie2Reviews.get(userID) < 3&& movie1Reviews.get(userID) < 3) //Found a common dislike.
					{
						dislikes++; 
					}
					
					else if(movie2Reviews.get(userID) > 3&& movie1Reviews.get(userID) > 3){ //Found a common like.
						likes++;
					}
				}
				
			}
			
			return 1 + totalReviewers - (likes + dislikes);
		}
		
		/**
		 * hashCode for testing equality.
		 */
		@Override
		public int hashCode() {
			
			return similarityGraph.hashCode();
		}
		
		/**
		 * Method to check if two SimilarityGraph objects are equal. 
		 * 
		 * @return	
		 * 		True if the SimilarityGraph objects have the same vertices mapped to the same 
		 * 		edges, false otherwise.
		 * 
		 */
		@Override
		public boolean equals(Object other) {
			
			if(other == null){
				return false;
			}
			
			if(other.getClass() != MovieGraph.class){
				return false;
			}

			return equals((SimilarityGraph) other);
		}
		
		public boolean equals(SimilarityGraph other){
			if(other == this){
				return true;
			}
			
			if(other.hashCode() != hashCode()){
				return false;
			}
			
			if(!other.getVertexAndEdges().equals(getVertexAndEdges())){
				return false;
			}
			
			return true;
		}


		/**
		 * 	Returns a hashMap representing the graph.
		 * 
		 * @return
		 * 		a hashMap containing all the movies on the graph mapped to a list of its vertex representation.
		 */
		public Object getVertexAndEdges() {
			return similarityGraph.getVertexAndEdges();
		}
}