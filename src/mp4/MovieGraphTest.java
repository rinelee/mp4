package mp4;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;



public class MovieGraphTest {
	
	//Add a vertex
	@Test
	public void testAddVertex(){
		Movie movie1 = new Movie(52340, "Unlimited Blade Works", 2014, "imdbUrl");
		MovieGraph graph = new MovieGraph();
		
		graph.addVertex(movie1);
		assertTrue(graph.getVertexAndEdges().containsKey(movie1));
		
	}

	
	@Test
	public void addDuplicateVertex(){
		Movie movie1 = new Movie(52340, "Unlimited Blade Works", 2014, "imdbUrl");
		MovieGraph graph = new MovieGraph();
		
		graph.addVertex(movie1);
		assertFalse(graph.addVertex(movie1));
	}
	
	//Add edges
	
	@Test
	public void addEdgeByMovie(){
		Movie movie1 = new Movie(52340, "Unlimited Blade Works", 2014, "imdbUrl");
		Movie movie2 = new Movie(23455, "Fate/stay Night(Feito/sutei naito)", 2012, "imdbUrl");
		MovieGraph graph = new MovieGraph();
		
		//Adding vertex and edges to graph
		assertTrue(graph.addVertex(movie1));
		assertTrue(graph.addEdge(movie1, movie2, 13));
		
		//Adding the same vertex and edge to graph(both method should return false)
		assertFalse(graph.addVertex(movie1));
		assertFalse(graph.addEdge(movie1,movie2,13));
		
		//Checking if the movies were added successfully using movie id's
		try {
			assertEquals(graph.getMovieId("Unlimited Blade Works"), 52340);
			assertEquals(graph.getMovieId("Fate/stay Night"), 23455);
			assertEquals(graph.getMovieId("Feito/sutei naito"), 23455);
			
		} catch (NoSuchMovieException e) {
			fail("Should not have thrown exception");
		}
		
	}
	
	
	@Test
	public void addEdgeByID(){
		Movie movie1 = new Movie(52340, "Unlimited Blade Works", 2014, "imdbUrl");
		Movie movie2 = new Movie(23455, "Fate/stay Night(Feito/sutei naito)", 2012, "imdbUrl");
		MovieGraph graph = new MovieGraph();
		
		//Adding vertex and edges to graph
		assertTrue(graph.addVertex(movie2));
		assertTrue(graph.addEdge(52340, 23455, 13));
		
		//Adding the same edge to graph(method should return false)
		assertFalse(graph.addEdge(movie1,movie2,13));
		
		
		//Checking if the movies were added successfully using movie id's
		try {
			assertEquals(graph.getMovieId("Unlimited Blade Works"), 52340);
			assertEquals(graph.getMovieId("Fate/stay Night"), 23455);
			assertEquals(graph.getMovieId("Feito/sutei naito"), 23455);
			
		} catch (NoSuchMovieException e) {
			fail("Should not have thrown exception");
		}
		
		
	}
	
	@SuppressWarnings("unused")
	@Test
	public void addNonExistentMovieEdge(){
		Movie movie1 = new Movie(52340, "Unlimited Blade Works", 2014, "imdbUrl");
		Movie movie2 = new Movie(23455, "Fate/stay Night(Feito/sutei naito)", 2012, "imdbUrl");
		
		MovieGraph graph = new MovieGraph();
		
		assertFalse(graph.addEdge(345, 23455, 13));
		assertFalse(graph.addEdge(52340, 9, 13));
		assertFalse(graph.addEdge(54, 9, 13));
	}
	
	//Find shortest path
	
	
	@Test
	public void findShortestDirectLength(){
		Movie movie1 = new Movie(52340, "Unlimited Blade Works", 2014, "imdbUrl");
		Movie movie2 = new Movie(23455, "Fate/stay Night(Feito/sutei naito)", 2012, "imdbUrl");
		MovieGraph graph = new MovieGraph();
		
		graph.addEdge(movie1, movie2, 6);
		
		try {
			assertEquals(graph.getShortestPathLength(52340, 23455), 6);

		} catch (NoSuchMovieException e) {
			fail("Should not have thrown exception.");
		} catch (NoPathException e) {
			fail("Should not have thrown exception.");
		}
	}
	@Test
	public void findShortestIndirectLength(){
		Movie movie1 = new Movie(52340, "Unlimited Blade Works", 2014, "imdbUrl");
		Movie movie2 = new Movie(23455, "Fate/stay Night(Feito/sutei naito)", 2012, "imdbUrl");
		Movie movie3 = new Movie(56778, "Mahoyo", 2344, "imdbUrl");
		MovieGraph graph = new MovieGraph();
		
		graph.addEdge(52340, 23455, 6);
		graph.addEdge(movie2, movie3, 1);
		graph.addEdge(movie3, movie1, 2);

		
		try {
			assertEquals(graph.getShortestPathLength(52340, 23455), 3);

		} catch (NoSuchMovieException e) {
			fail("Should not have thrown exception.");
		} catch (NoPathException e) {
			fail("Should not have thrown exception.");
		}
	}
	
	@Test
	public void testNoPathLength(){
		Movie movie1 = new Movie(1340, "Unlimited Blade Works", 2014, "imdbUrl");
		Movie movie2 = new Movie(23455, "Fate/stay Night(Feito/sutei naito)", 2012, "imdbUrl");
		Movie movie3 = new Movie(3, "Steins;Gate", 2010, "imdbUrl");
		MovieGraph graph = new MovieGraph();
		
		graph.addEdge(movie1, movie2, 6);
		graph.addVertex(movie3);
	
		try {
			graph.getShortestPathLength(23455, 3);
			fail("Should have thrown exception");
		} catch (NoSuchMovieException e) {
			fail("Should not fail here.");
		} catch (NoPathException e) {
		}

	}
	
	//Find Shortest Path
	@Test
	public void findShortestDirectPath(){
		Movie movie1 = new Movie(52340, "Unlimited Blade Works", 2014, "imdbUrl");
		Movie movie2 = new Movie(23455, "Fate/stay Night(Feito/sutei naito)", 2012, "imdbUrl");
		Movie movie3 = new Movie(56778, "Mahoyo", 2344, "imdbUrl");
		MovieGraph graph = new MovieGraph();
		List<Movie> vertices1 = new  ArrayList<Movie>();
		
		//Creating a few edges.
		graph.addEdge(movie1,movie2,1);
		graph.addEdge(movie2, movie3, 1);
		graph.addEdge(movie3, movie1, 2);

		
		//Checks if correct list of vertices is returned
		try {
			vertices1 = graph.getShortestPath(52340, 23455);
		} catch (NoSuchMovieException e) {
			fail("Should not have thrown exception.");
		} catch (NoPathException e) {
			fail("Should not have thrown exception.");
		}
		
		List<Movie> expectedPath = new ArrayList<Movie>();
		expectedPath.add(movie1);
		expectedPath.add(movie2);
		
		assertEquals(vertices1, expectedPath);
	}
	
	@Test
	public void findShortestIndirectPath(){
		Movie movie1 = new Movie(52340, "Unlimited Blade Works", 2014, "imdbUrl");
		Movie movie2 = new Movie(23455, "Fate/stay Night(Feito/sutei naito)", 2012, "imdbUrl");
		Movie movie3 = new Movie(56778, "Mahoyo", 2344, "imdbUrl");
		MovieGraph graph = new MovieGraph();
		List<Movie> vertices2 = new ArrayList<Movie>();
		
		//Creating a few edges
		graph.addEdge(movie2, movie3, 1);
		graph.addEdge(movie3, movie1, 2);
		
		
		//Checks if correct list of vertices is returned
		try {
			vertices2 = graph.getShortestPath(23455, 52340);
		} catch (NoSuchMovieException e) {
			fail("Should not have thrown exception.");
		} catch (NoPathException e) {
			fail("Should not have thrown exception.");
		}
		
		List<Movie> expectedPath = new ArrayList<Movie>();
		expectedPath.add(movie2);
		expectedPath.add(movie3);
		expectedPath.add(movie1);
		
		assertEquals(vertices2, expectedPath);
	}
	
	@Test
	public void testNoMovie(){
		MovieGraph graph = new MovieGraph();
		
		try {
			graph.getShortestPath(1111, 34556);
			fail("Should have thrown exception");
		} catch (NoSuchMovieException e) {
		} catch (NoPathException e) {
			fail("Should not fail here.");
		}
		
	}
	
	@Test
	public void testNoPath(){
		Movie movie1 = new Movie(1340, "Unlimited Blade Works", 2014, "imdbUrl");
		Movie movie2 = new Movie(23455, "Fate/stay Night(Feito/sutei naito)", 2012, "imdbUrl");
		Movie movie3 = new Movie(3, "Steins;Gate", 2010, "imdbUrl");
		MovieGraph graph = new MovieGraph();
		
		graph.addEdge(movie1, movie2, 6);
		graph.addVertex(movie3);
	
		try {
			graph.getShortestPath(23455, 3);
			fail("Should have thrown exception");
		} catch (NoSuchMovieException e) {
			fail("Should not fail here.");
		} catch (NoPathException e) {
		}

	}
	
	//Search and find a movie.
	@Test
	public void locateMoviebyTitle(){
		Movie movie1 = new Movie(52340, "Unlimited Blade Works", 2014, "imdbUrl");
		Movie movie2 = new Movie(23455, "Fate/stay Night(Feito/sutei naito)", 2012, "imdbUrl");
		
		MovieGraph graph = new MovieGraph();
		graph.addVertex(movie1);
		graph.addVertex(movie2);
		
		try {
			assertEquals(graph.getMovieId("Unlimited Blade Works"), 52340);
			assertEquals(graph.getMovieId("Fate/stay Night"), 23455);
			assertEquals(graph.getMovieId("Feito/sutei naito"), 23455);
			
		} catch (NoSuchMovieException e) {
			fail("Should not have thrown exception");
		}
	}
	
	@Test
	public void searchNonExistentTitle(){
		MovieGraph graph = new MovieGraph();
		
		try{
			graph.getMovieId("Witch of the Holy Night");
			fail("Should have thrown exception.");
		}
		catch (NoSuchMovieException e) {
		}
		
	}
	
	@Test
	public void incompleteMatch(){
		Movie movie1 = new Movie(23455, "Fate/stay Night(Feito/sutei naito)", 2012, "imdbUrl");
		MovieGraph graph = new MovieGraph();
		
		graph.addVertex(movie1);
		
		try{
			graph.getMovieId("Fate");
			fail("Should have thrown exception.");
		}
		catch (NoSuchMovieException e) {
		}
		
	}
	
	@Test
	public void searchEmptyTitle(){
		MovieGraph graph = new MovieGraph();
		
		try{
			graph.getMovieId("");
			fail("Should have thrown exception.");
		}
		catch (NoSuchMovieException e) {
		}
	}
	
	//Test Object Equality
	@Test
	public void equalityTest(){
		MovieGraph graph1 = new MovieGraph();
		MovieGraph graph2 = new MovieGraph();
		MovieGraph graph3 = new MovieGraph();
		
		Movie movie1 = new Movie(233, "Heavens Feel", 2014, "url");
		Movie movie2 = new Movie(955, "Detective Conan", 1995, "url");
		Movie movie3 = new Movie(233, "Heavens Feel", 2014, "url");
		Movie movie4 = new Movie(955, "Detective Conan", 1995, "url");
		
		graph1.addEdge(movie1, movie2, 5);
		graph2.addEdge(movie3, movie4, 5);
		graph3.addEdge(movie1, movie2, 5);
		
		assertEquals(graph1, graph2);
		assertEquals(graph2, graph3);
	}

}
