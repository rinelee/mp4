	package mp4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;


public class MovieGraph {
	
	//This class represents an undirected graph of movies as vertices and edges. 
	//A vertex can be connected to n other vertices, via n edges.


	//Abstraction Function:
	//AF(r) = a graph, graphValues, such that every vertex on the graph is recorded and mapped to
	//every edge connecting it to another vertex. Two MovieGraphs are equal if they have the
	//same values mapped to the same edges.

	//Rep Invariants:
	//graphValues != null
	//Every key and value in graphValues != null
	//Every edge will be mapped to both of its vertices.
	//Every key in graphValues is also a key in vertexInfo.
	
	//Note: The rep will not be checked after all mutator methods, as this can be expensive with 
	//a thousand movies.


	private HashMap<Movie, Vertex> graphValues = new HashMap<Movie, Vertex>();
	private HashMap<String, Integer> vertexInfo = new HashMap<String, Integer>(); 
	
	/**
	 * Add a new movie to the graph. If the movie already exists in the graph
	 * then this method will return false. Otherwise this method will add the
	 * movie to the graph and return true.
	 * 
	 * @param movie
	 *            the movie to add to the graph. Requires that movie != null.
	 * @return true if the movie was successfully added and false otherwise.
	 * @modifies this by adding the movie to the graph if the movie did not
	 *           exist in the graph.
	 */
	public boolean addVertex(Movie movie) {
		if(!graphValues.containsKey(movie)){
			graphValues.put(movie, new Vertex(movie)); //Vertex has no edges at the moment.
			vertexInfo.put(movie.getName(), movie.getID()); //Store for retrieval.
			return true;
		}
		
		return false;
	}

	/**
	 * 	Returns a hashMap representing the graph.
	 * 
	 * @return
	 * 		a hashMap containing all the movies on the graph mapped to a list of its vertex representation.
	 */
	public HashMap<Movie, Vertex> getVertexAndEdges(){
			
		return new HashMap<Movie, Vertex>(graphValues);
	}
	/**
	 * Add a new edge to the graph. If the edge already exists in the graph then
	 * this method will return false. Otherwise this method will add the edge to
	 * the graph and return true.
	 * 
	 * @param movie1
	 *            one end of the edge being added. Requires that m1 != null.
	 * @param movie2
	 *            the other end of the edge being added. Requires that m2 !=
	 *            null. Also require that m1 is not equal to m2 because
	 *            self-loops are not meaningful in this graph.
	 * @param edgeWeight
	 *            the weight of the edge being added. Requires that edgeWeight >
	 *            0.
	 * @return true if the edge was successfully added and false otherwise.
	 * @modifies this by adding the edge to the graph if the edge did not exist
	 *           in the graph.
	 */
	public boolean addEdge(Movie movie1, Movie movie2, int edgeWeight) {
		boolean status;
		
		addVertex(movie1);
		addVertex(movie2);
		
		Vertex vertex1 = graphValues.get(movie1);
		Vertex vertex2 = graphValues.get(movie2);
		
		status = vertex1.addEdge(vertex2, edgeWeight);
		
		if(status){ //Ensure that the status is true.
			status = vertex2.addEdge(vertex1, edgeWeight);
		}

		return status;
	}
	

	/**
	 * Add a new edge to the graph. If the edge already exists in the graph then
	 * this method should return false. Otherwise this method should add the
	 * edge to the graph and return true.
	 * 
	 * @param movieId1
	 *            the movie id for one end of the edge being added. Requires
	 *            that m1 != null.
	 * @param movieId2
	 *            the movie id for the other end of the edge being added.
	 *            Requires that m2 != null. Also require that m1 is not equal to
	 *            m2 because self-loops are not meaningful in this graph.
	 * @param edgeWeight
	 *            the weight of the edge being added. Requires that edgeWeight >
	 *            0.
	 * @return true if the edge was successfully added and false otherwise.
	 * @modifies this by adding the edge to the graph if the edge did not exist
	 *           in the graph.
	 */
	public boolean addEdge(int movieId1, int movieId2, int edgeWeight) {
		Movie movie1;
		Movie movie2;
		
		try {
			movie1 = Movie.retrieveMovieByID(movieId1);
			movie2 = Movie.retrieveMovieByID(movieId2);
		} catch (NoSuchMovieException e) {
			
			return false;
		}
		
		return addEdge(movie1, movie2, edgeWeight);
	}	

	/**
	 * Return the length of the shortest path between two movies in the graph.
	 * Throws an exception if the movie ids do not represent valid vertices in
	 * the graph.
	 * 
	 * @param moviedId1
	 *            the id of the movie at one end of the path.
	 * @param moviedId2
	 *            the id of the movie at the other end of the path.
	 * @throws NoSuchMovieException
	 *             if one or both arguments are not vertices in the graph.
	 * @throws NoPathException
	 *             if there is no path between the two vertices in the graph.
	 * 
	 * @return the length of the shortest path between the two movies
	 *         represented by their movie ids.
	 */
	public int getShortestPathLength(int movieId1, int movieId2)
			throws NoSuchMovieException, NoPathException {
		Movie movie1 = Movie.retrieveMovieByID(movieId1);
		Movie movie2 = Movie.retrieveMovieByID(movieId2);
		
		if(!graphValues.containsKey(movie1) || !graphValues.containsKey(movie2)){
			throw new NoSuchMovieException();
		}
		
		Vertex source = graphValues.get(movie1);
		Vertex target = graphValues.get(movie2);
		Queue<Vertex> vertexes = new LinkedList<Vertex>();
		
		
		//Based on Dijkstra's Algorithm described by http://en.wikipedia.org/wiki/Dijkstra's_algorithm
		//and http://www.algolist.com/code/java/Dijkstra's_algorithm
	
		source.setDistance(0);
		for(Vertex currVertex: graphValues.values()){
			if(currVertex != source){
				currVertex.setDistance(Double.POSITIVE_INFINITY);
			}
			
			vertexes.add(currVertex);
		}
		
		while(!vertexes.isEmpty()){
			Vertex trialTarget = vertexes.remove();
			
			HashMap<Vertex, Integer> adjacencies = trialTarget.getDirectPaths();
			
			for(Vertex currVertex: adjacencies.keySet()){
				double weight = adjacencies.get(currVertex);
				double trialDistance = trialTarget.getDistance() + weight;
				
				if(trialDistance < currVertex.getDistance()){
					vertexes.remove(currVertex);
					
					currVertex.setDistance(trialDistance);
					currVertex.setPreNode(trialTarget);
					
					vertexes.add(currVertex); //Replace the node if it's the minimum.
				}
			}
			
		}
		
		//End Dijkstra's Algorithm.
		
		if(target.getDistance() == Double.POSITIVE_INFINITY){
			throw new NoPathException();
		}
		
		return (int) target.getDistance();
		
	
	}

	/**
	 * Return the shortest path between two movies in the graph. Throws an
	 * exception if the movie ids do not represent valid vertices in the graph.
	 * 
	 * @param moviedId1
	 *            the id of the movie at one end of the path.
	 * @param moviedId2
	 *            the id of the movie at the other end of the path.
	 * @throws NoSuchMovieException
	 *             if one or both arguments are not vertices in the graph.
	 * @throws NoPathException
	 *             if there is no path between the two vertices in the graph.
	 * 
	 * @return the shortest path, as a list, between the two movies represented
	 *         by their movie ids. This path begins at the movie represented by
	 *         movieId1 and ends with the movie represented by movieId2.
	 */
	public List<Movie> getShortestPath(int movieId1, int movieId2)
			throws NoSuchMovieException, NoPathException {
		Movie movie1 = Movie.retrieveMovieByID(movieId1);
		Movie movie2 = Movie.retrieveMovieByID(movieId2);
		
		if(!graphValues.containsKey(movie1) || !graphValues.containsKey(movie2)){
			throw new NoSuchMovieException();
		}
		
		Vertex source = graphValues.get(movie1);
		Vertex target = graphValues.get(movie2);
		Queue<Vertex> vertexes = new LinkedList<Vertex>();
		
		
		//Based on Dijkstra's Algorithm described by http://en.wikipedia.org/wiki/Dijkstra's_algorithm
		//and http://www.algolist.com/code/java/Dijkstra's_algorithm
	
		source.setDistance(0);
		for(Vertex currVertex: graphValues.values()){
			if(currVertex != source){
				currVertex.setDistance(Double.POSITIVE_INFINITY);
			}
			
			vertexes.add(currVertex);
		}
		
		while(!vertexes.isEmpty()){
			Vertex trialTarget = vertexes.remove();
			
			HashMap<Vertex, Integer> adjacencies = trialTarget.getDirectPaths();
			
			for(Vertex currVertex: adjacencies.keySet()){
				double weight = adjacencies.get(currVertex);
				double trialDistance = trialTarget.getDistance() + weight;
				
				if(trialDistance < currVertex.getDistance()){
					vertexes.remove(currVertex);
					
					currVertex.setDistance(trialDistance);
					currVertex.setPreNode(trialTarget);
					
					vertexes.add(currVertex); //Replace the node if it's the minimum.
				}
			}
			
		}
		
		//End Dijkstra's Algorithm.
		
		if(target.getDistance() == Double.POSITIVE_INFINITY){
			throw new NoPathException();
		}	
		
		Vertex currVertex = target;
		List<Movie> Path = new ArrayList<Movie>();
	
		while(!currVertex.equals(source)){
			Path.add(currVertex.getMovie());
			currVertex = currVertex.getPreNode();
		}
		
		Path.add(currVertex.getMovie()); //Add the source movie.
		
		Collections.reverse(Path);
		
		return Path;
		
	}

	/**
	 * Returns the movie id given the name of the movie. For movies that are not
	 * in English, the name contains the English transliteration original name
	 * and the English translation. A match is found if any one of the two
	 * variations is provided as input. Typically the name string has <English
	 * Translation> (<English Transliteration>) for movies that are not in
	 * English.
	 * 
	 * @param name
	 *            the movie name for the movie whose id is needed.
	 * @return the id for the movie corresponding to the name. If an exact match
	 *         is not found then return the id for the movie with the best match
	 *         in terms of translation/transliteration of the movie name. Note that an
	 *         exact match of the translation/transliteration  or the start of the English name 
	 *         is required.
	 * @throws NoSuchMovieException
	 *             if the name does not match any movie in the graph or is an empty string.
	 */
	public int getMovieId(String name) throws NoSuchMovieException {
		if(name.isEmpty()){
			throw new NoSuchMovieException();
		}
		if(vertexInfo.containsKey(name)){
			return vertexInfo.get(name);
		}
		
		Set<String> titles = vertexInfo.keySet();
		
		for(String currTitle: titles){
			if(currTitle.startsWith(name + " (") || currTitle.startsWith(name + "(")){ //The English name is an exact match.
				return vertexInfo.get(currTitle);
			}
			
			else if(currTitle.endsWith("(" + name + ")")){ //The translation/transliteration is an exact match.
				return vertexInfo.get(currTitle);
			}
		}
		
		//This point will be reached only if no matching titles were found.
		throw new NoSuchMovieException();
	}

	
	/**
	 * Method to check if two MovieGraph objects are equal. 
	 * 
	 * @return	
	 * 		True if the MovieGraph objects have the same vertices mapped to the same 
	 * 		edges, false otherwise.
	 * 
	 */
	@Override
	public boolean equals(Object other) {
		
		if(other == null){
			return false;
		}
		
		if(other.getClass() != MovieGraph.class){
			return false;
		}

		return equals((MovieGraph) other);
	}
	
	public boolean equals(MovieGraph other){
		if(other == this){
			return true;
		}
		
		if(other.hashCode() != hashCode()){
			return false;
		}
		
		if(!other.getVertexAndEdges().equals(graphValues)){
			return false;
		}
		
		return true;
	}

	/**
	 * hashCode for testing equality.
	 */
	@Override
	public int hashCode() {
		int hashValue = 0;
		Set<Movie> vertices = graphValues.keySet();
		
		for(Movie currMovie: vertices){
			hashValue += currMovie.hashCode();
		}
		
		return hashValue;
	}

}
